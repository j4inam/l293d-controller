import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-set-new-rpm-dialog',
  templateUrl: './set-new-rpm-dialog.component.html',
  styleUrls: ['./set-new-rpm-dialog.component.css']
})
export class SetNewRpmDialogComponent implements OnInit {
  dialogTitle: string;

  rpmToSet = new FormControl('', [Validators.required,
                                  Validators.max(1000),
                                  Validators.min(0)]);

  constructor(private dialogRef: MatDialogRef<SetNewRpmDialogComponent>,
                @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit() {
    this.dialogTitle = this.data.title;
  }

}
