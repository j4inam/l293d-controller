import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetNewRpmDialogComponent } from './set-new-rpm-dialog.component';

describe('SetNewRpmDialogComponent', () => {
  let component: SetNewRpmDialogComponent;
  let fixture: ComponentFixture<SetNewRpmDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetNewRpmDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetNewRpmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
