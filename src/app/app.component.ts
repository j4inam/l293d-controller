import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormControl } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { MatDialog, MatSnackBar } from "@angular/material";
import { SetNewRpmDialogComponent } from "./set-new-rpm-dialog/set-new-rpm-dialog.component";
import { SwUpdate } from "@angular/service-worker";
import { DeviceConfigDialogComponent } from "./device-config-dialog/device-config-dialog.component";
import { AddDeviceDialogComponent } from "./add-device-dialog/add-device-dialog.component";
import { Device } from "./models/device";
import { ActionConfirmDialogComponent } from "./action-confirm-dialog/action-confirm-dialog.component";
import * as _ from "lodash";
import { MotorService } from './services/motor.service';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  motorState = false;
  systemConnected = false;
  dialogRef;
  currentRpm = 1000;
  isDarkMode = false;
  savedDevices: Device[];
  selectedDevice: Device;
  showStartSystemSpinner  = false;
  realtimeRPM = 0;

  constructor(
    private spinner: NgxSpinnerService,
    private swUpdate: SwUpdate,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private motorService: MotorService
  ) {
    this.swUpdate.available.subscribe(evt => {
      this.swUpdate.checkForUpdate().then(() => {
        const snack = this.snackbar.open("App update available", "Update now");
        snack.onAction().subscribe(() => {
          window.location.reload();
        });
      });
    });
  }

  ngOnInit(): void {
    this.isDarkMode = window.localStorage.getItem("dark-mode") == "true";
    this.savedDevices = localStorage.getItem("devices")
      ? JSON.parse(localStorage.getItem("devices"))
      : [];

    this.getSelectedDevice();

    const body = document.getElementsByClassName("mat-app-background")[0];
    if (this.isDarkMode) {
      body.classList.remove("light-theme");
      body.classList.add("dark-theme");
    }
  }

  // ********** UI & Device Config **********

  toggleDarkMode() {
    const body = document.getElementsByClassName("mat-app-background")[0];
    if (this.isDarkMode) {
      body.classList.remove("dark-theme");
      body.classList.add("light-theme");
    } else {
      body.classList.remove("light-theme");
      body.classList.add("dark-theme");
    }
    this.isDarkMode = !this.isDarkMode;
    window.localStorage.setItem("dark-mode", this.isDarkMode.toString());
  }

  getSelectedDevice() {
    this.selectedDevice = null;
    this.savedDevices = localStorage.getItem("devices")
      ? JSON.parse(localStorage.getItem("devices"))
      : [];
    if (this.savedDevices.length > 0) {
      this.selectedDevice = _.find(this.savedDevices, { selected: true });
      if (!this.selectedDevice) {
        this.selectedDevice = this.savedDevices[0];
      }
    }
  }

  showDeviceConfigDialog() {
    this.dialogRef = this.dialog.open(DeviceConfigDialogComponent, {
      width: "600px",
      closeOnNavigation: true
    });

    this.dialogRef.afterClosed().subscribe(res => {
      if (res) {
        if (res.action == "add-device") {
          this.showAddDeviceDialog();
        }

        if (res.action == "remove-device") {
          this.deleteDevice(res.device);
        }
      } else {
        this.getSelectedDevice();
      }
    });
  }

  showAddDeviceDialog() {
    this.dialogRef = this.dialog.open(AddDeviceDialogComponent, {
      width: "600px",
      closeOnNavigation: true
    });

    this.dialogRef.afterClosed().subscribe((deviceData: Device) => {
      if (deviceData) {
        deviceData.selected = false;
        deviceData.addedOn = Date.now();
        deviceData.id = Date.now();
        if (
          !deviceData.name ||
          deviceData.name == " " ||
          deviceData.name == ""
        ) {
          deviceData.name = deviceData.ip;
        }

        if (this.savedDevices.length == 0) {
          deviceData.selected = true;
        }
        this.savedDevices.push(deviceData);
        localStorage.setItem("devices", JSON.stringify(this.savedDevices));
        this.snackbar.open("Device Added Successfully!", "Dismiss",  {
          duration: 3000
        });
        this.getSelectedDevice();
      }
    });
  }

  deleteDevice(device: Device) {
    this.dialogRef = this.dialog.open(ActionConfirmDialogComponent, {
      width: "500px",
      closeOnNavigation: true,
      data: {
        title: `Delete Device`,
        messageLine1: `Confirm Delete Device - ${device.name} ?`,
        messageLine2: "This cannot be undone."
      }
    });

    this.dialogRef.afterClosed().subscribe(res => {
      if (res) {
        if (this.savedDevices.length > 0) {
          const indexToDelete = _.findIndex(this.savedDevices, {
            id: device.id
          });
          this.savedDevices.splice(indexToDelete, 1);
          localStorage.setItem("devices", JSON.stringify(this.savedDevices));
          this.snackbar.open("Device Removed Successfully!", "Dismiss",  {
            duration: 3000
          });
          this.getSelectedDevice();
        }
      }
    });
  }

  // ********** Motor Operation Functions **********
  toggleMasterSwitch() {
    if (!this.systemConnected) {
      this.startSystem();
    } else {
      this.showStartSystemSpinner = true;
      setTimeout(() => {
        this.systemConnected = false;
        if (this.motorState) {
          this.toggleMotor();
        }
        this.showStartSystemSpinner = false;
      }, 500);
    }
  }

  startSystem() {
    this.spinner.show();
    this.showStartSystemSpinner = true;
    this.motorService.startSystem().subscribe(res => {
      this.spinner.hide();
      this.showStartSystemSpinner = false;
      if (res && res.motor_status != undefined && res.set_rpm != undefined) {
        console.log("inside system conncted true");
        this.motorState = res.motor_status == 1;
        this.currentRpm = res.set_rpm;
        this.systemConnected = true;
        this.getSystemStatus();
      }
    }, () => {
      this.spinner.hide();
      this.showStartSystemSpinner = false;
      this.systemConnected = false;
    });
  }

  toggleMotor() {
    this.spinner.show();
    this.motorService.toggleMotor().subscribe(res => {
      this.spinner.hide();
      if (res && res.motor_status != undefined && res.set_rpm != undefined) {
        this.motorState = res.motor_status == 1;
        this.currentRpm = res.set_rpm;
      }
    }, () => {
      this.spinner.hide();
    });
  }

  showSetRPMDialog() {
    this.dialogRef = this.dialog.open(SetNewRpmDialogComponent, {
      width: "450px",
      closeOnNavigation: true,
      data: {
        title: "Set New RPM"
      }
    });

    this.dialogRef.afterClosed().subscribe(rpmToSet => {
      if (rpmToSet) {
        this.spinner.show();
        this.motorService.setRPM(rpmToSet).subscribe(res => {
          this.spinner.hide();
          if (res && res.motor_status != undefined && res.set_rpm != undefined) {
            this.motorState = res.motor_status == 1;
            this.currentRpm = res.set_rpm;
          }
        }, () => {
          this.spinner.hide();
        });
      }
    });
  }

  getSystemStatus() {
    this.motorService.getStatus().subscribe(res => {
      if (res && res.current_rpm != undefined) {
        // this.motorState = res.motor_status == 1;
        this.realtimeRPM = res.current_rpm;
      }

      setTimeout(() => {
        if (this.systemConnected) {
          this.getSystemStatus();
        }
      }, 5000);
    });
  }
}
