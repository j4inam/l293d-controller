import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getData(url: string, headers?: any) {
    return this.http.get(url, headers);
  }

  putData(url: string, data: any, headers?: any) {
    return this.http.put(url, data, headers);
  }

  postData(url: string, data: any, headers?: any) {
    return this.http.post(url, data, headers);
  }

  deleteData(url: string, headers?: any) {
    return this.http.delete(url, headers);
  }
}
