import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, tap, catchError } from "rxjs/operators";
import { MatSnackBar } from '@angular/material';
import { MotorResponse } from '../models/motor-response';

@Injectable({
  providedIn: 'root'
})
export class MotorService {

  constructor(private dataService: DataService, private snackbar: MatSnackBar) { }

  getStatus() {
    return this.dataService.getData('/status').pipe(  // {"current_rpm": 450 }
      tap(console.log),
      map((res: MotorResponse) => {
        console.log("Status Response", res);
        return res;
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status != 0) {
          console.log("status err", err.message);
          this.snackbar.open("Something went wrong. Try again!", "", {
            duration: 3000
          });
        }
        return of(null);
      })
    );
  }

  startSystem() {
    return this.dataService.getData('/start-system').pipe(  // {"motor_status": 1 or 0, "set_rpm": 450 }
      tap(console.log),
      map((res: MotorResponse) => {
        console.log("Status Response", res);
        return res;
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status != 0) {
          // console.log("status err", err.message);
          this.snackbar.open("Something went wrong. Try again!", "", {
            duration: 3000
          });
        }
        return of(null);
      })
    );
  }

  toggleMotor() {
    return this.dataService.getData('/motor-toggle').pipe(  // {"motor_status": 1 or 0, "set_rpm": 450 }
      tap(console.log),
      map((res: MotorResponse) => {
        console.log("Status Response", res);
        return res;
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status != 0) {
          console.log("status err", err.message);
          this.snackbar.open("Something went wrong. Try again!", "", {
            duration: 3000
          });
        }
        return of(null);
      })
    );
  }

  setRPM(rpm: number) {
    return this.dataService.getData(`/set-rpm?rpm=${rpm}`).pipe(  // {"motor_status": 1 or 0, "set_rpm": 450 }
      tap(console.log),
      map((res: MotorResponse) => {
        console.log("Status Response", res);
        return res;
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status != 0) {
          console.log("status err", err.message);
          this.snackbar.open("Something went wrong. Try again!", "", {
            duration: 3000
          });
        }
        return of(null);
      })
    );
  }
}
