import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { MatSnackBar } from "@angular/material";
import * as _ from "lodash";

@Injectable({
  providedIn: "root"
})
export class InterceptorService {
  constructor(private snackbar: MatSnackBar) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const savedDevices = localStorage.getItem("devices")
      ? JSON.parse(localStorage.getItem("devices"))
      : [];
    const selectedDevice = _.find(savedDevices, "selected");
    console.log("Req", req, req.url, req.headers);
    req = req.clone({
      url: "http://" + (selectedDevice.ip.indexOf(":") == -1 ? (selectedDevice.ip + ":80") : selectedDevice.ip) + req.url
    });
    console.log("INTERCEPTOR URL", req.url);
    return next.handle(req).pipe(
      catchError(err => {
        if (err instanceof HttpErrorResponse) {
          console.log("err", err);
          if (err.status == 0) {
            this.snackbar.open("Could not reach device!", "", {
              duration: 3000
            });
            return throwError(err);
          }
        }
        return throwError(err);
      })
    );
  }
}
