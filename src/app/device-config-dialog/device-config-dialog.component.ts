import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Device } from "../models/device";
import * as _ from "lodash";

@Component({
  selector: "app-device-config-dialog",
  templateUrl: "./device-config-dialog.component.html",
  styleUrls: ["./device-config-dialog.component.css"]
})
export class DeviceConfigDialogComponent implements OnInit {
  savedDevices: Device[];

  constructor(
    private dialogRef: MatDialogRef<DeviceConfigDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {}

  ngOnInit() {
    this.savedDevices = localStorage.getItem("devices")
      ? JSON.parse(localStorage.getItem("devices"))
      : [];
    this.savedDevices = _.sortBy(this.savedDevices, "addedOn").reverse();
  }

  toggleSort(param) {
    switch (param) {
      case "name":
        this.savedDevices = _.sortBy(this.savedDevices, "name");
        break;

      case "time":
        this.savedDevices = _.sortBy(this.savedDevices, "addedOn").reverse();
        break;
    }
  }

  removeDevice(device: Device) {
    this.dialogRef.close({action: 'remove-device', device: device });
  }

  updateSelectedDevice(id: number) {
    this.savedDevices.forEach(device => {
      if (id != device.id) {
        device.selected = false;
      } else {
        device.selected = true;
      }
      localStorage.setItem('devices', JSON.stringify(this.savedDevices));
    });

    this.dialogRef.close();
  }
}
