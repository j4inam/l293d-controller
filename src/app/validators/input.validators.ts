import { AbstractControl } from "@angular/forms";
import { ValidationErrors } from '@angular/forms/src/directives/validators';


export class InputValidators {
    static IPAddress(control: AbstractControl): ValidationErrors | null {
        // tslint:disable-next-line:max-line-length
        const regex = new RegExp(`^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$`);
        if (control.value && !regex.test(control.value)) {
            return { IPAddress: true };
        }
        return null;
    }
}
