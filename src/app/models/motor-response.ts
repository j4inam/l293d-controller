export interface MotorResponse {
  motor_status?: number;
  current_rpm?: number;
  set_rpm?: number;
}

