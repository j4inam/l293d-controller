export interface Device {
  id: number;
  name: string;
  ip: string;
  addedOn: any;
  selected: boolean;
}
